import {DefaultTheme} from 'react-native-paper';

export const appTheme = {
  ...DefaultTheme,
  colors: {
    ...DefaultTheme.colors,
    primary: '#1B75BC',
    accent: 'white',
  },
};
