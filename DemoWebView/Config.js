export const apps = {
  about: {
    screenTitle: 'About IRM',
    URL: 'https://www.ironmountain.com/in',
  },
  angular: {
    screenTitle: 'Angular',
    URL: 'https://angular.io/',
  },
  reactNative: {
    screenTitle: 'React Native',
    URL: 'https://reactnative.dev/docs/getting-started',
  },
};

export const remoteConfig = {
  links: 'LINKS',
};
