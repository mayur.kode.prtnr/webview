import React, {useState, useRef, useEffect} from 'react';
import {StyleSheet, SafeAreaView, View} from 'react-native';
import {Text, ActivityIndicator, Button} from 'react-native-paper';
import WebView from 'react-native-webview';
import {appTheme} from '../Theme';

export default function Webapp({navigation, route}) {
  const [canGoBack, setCanGoBack] = useState(false);
  const [canGoForward, setCanGoForward] = useState(false);
  const [currentUrl, setCurrentUrl] = useState('');

  const webviewRef = useRef(null);

  const backButtonHandler = () => {
    if (webviewRef.current) webviewRef.current.goBack();
  };

  const frontButtonHandler = () => {
    if (webviewRef.current) webviewRef.current.goForward();
  };

  useEffect(() => {
    console.log(route.params);
    if (route.params) {
      setCurrentUrl(route.params.URL);
    }
  }, [route]);

  return (
    <SafeAreaView style={styles.flexContainer}>
      <WebView
        source={{uri: currentUrl}}
        startInLoadingState={true}
        renderLoading={() => (
          <ActivityIndicator
            animating={true}
            color={appTheme.colors.primary}
            size="large"
            style={styles.flexContainer}
          />
        )}
        ref={webviewRef}
        onNavigationStateChange={navState => {
          setCanGoBack(navState.canGoBack);
          setCanGoForward(navState.canGoForward);
          setCurrentUrl(navState.url);
        }}
      />

      <View style={styles.tabBarContainer}>
        <Button onPress={backButtonHandler}> Back</Button>
        <Button onPress={frontButtonHandler}> Forward</Button>
      </View>
    </SafeAreaView>
  );
}

const styles = StyleSheet.create({
  flexContainer: {
    flex: 1,
  },
  tabBarContainer: {
    padding: 5,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
});
