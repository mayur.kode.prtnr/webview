import React from 'react';
import {StyleSheet, View} from 'react-native';
import firebase from 'react-native-firebase';
import {Button, Text} from 'react-native-paper';
import {apps, remoteConfig} from '../Config';

export default function Home({navigation}) {
  const onPressRN = () => {
    navigation.navigate('Webapp', apps.reactNative);
  };

  const onPressFlutter = () => {
    navigation.navigate('Webapp', apps.angular);
  };

  const onPressAbout = () => {
    // navigation.navigate('About', {name: 'about'});
    navigation.navigate('Webapp', apps.about);
  };

  const onPressGetRemoteConfig = () => {
    // console.log('onPressGetRemoteConfig');

    if (__DEV__) {
      firebase.config().enableDeveloperMode;
    }

    // setting default config
    firebase.config().setDefaults({
      LINKS: 'Giving Defaults',
    });

    firebase
      .config()
      .fetch()
      .then(() => {
        return firebase.config().activateFetched();
      })
      .then(activated => {
        // console.log('activated :', activated);
        if (!activated) {
          // console.log('Not found');
          return firebase.config().getValue('LINKS');
        }
      })
      .then(snapshot => {
        // console.log('snapshot :', snapshot);
        const msg = snapshot.val();
        console.log(msg);
      })
      .catch(e => {
        console.log(e);
      });
  };

  return (
    <View style={styles.flexContainer}>
      <Button onPress={onPressRN} mode="contained" style={styles.button}>
        React Native
      </Button>
      <Button onPress={onPressFlutter} mode="contained" style={styles.button}>
        Angular
      </Button>
      <Button onPress={onPressAbout} mode="contained" style={styles.button}>
        About
      </Button>

      <Button
        onPress={onPressGetRemoteConfig}
        mode="contained"
        style={styles.button}>
        Get Config
      </Button>
    </View>
  );
}

const styles = StyleSheet.create({
  flexContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  button: {
    width: '50%',
    margin: 20,
  },
});
